create table documents (
    id integer primary key,
    name integer
);

create table dictionary_values (
   id integer primary key,
   name varchar
);

create table some_entities (
    id integer primary key,
    document_id integer,
    dictionary_value_id integer,
    sort_order varchar,
    test_id integer unique,
    test_name varchar
);

ALTER TABLE some_entities
    ADD FOREIGN KEY (document_id) REFERENCES documents(id);

ALTER TABLE some_entities
    ADD FOREIGN KEY (dictionary_value_id) REFERENCES dictionary_values(id);
package com.javapadawan.alextesttask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AlexTestTaskApplication {
	public static void main(String[] args) {
		SpringApplication.run(AlexTestTaskApplication.class, args);

	}

}

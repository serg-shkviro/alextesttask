package com.javapadawan.alextesttask.repository;

import com.javapadawan.alextesttask.domain.SomeEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SomeEntityRepository extends JpaRepository<SomeEntity, Long> {

}

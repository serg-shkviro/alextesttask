package com.javapadawan.alextesttask.controller;

import com.javapadawan.alextesttask.domain.SomeEntity;
import com.javapadawan.alextesttask.service.SomeEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.http.HttpResponse;
import java.util.List;

@RestController
@RequestMapping("/test")
public class SomeEntityController {
    @Autowired
    private SomeEntityService service;

    @GetMapping("/all")
    public List<SomeEntity> getAll() {
        return service.getAll();
    }

    @GetMapping("/one/{id}")
    public SomeEntity getOne(@PathVariable long id) {
        if (service.getById(id) != null) {
            return service.getById(id);
        }
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "nothing found");
    }

    @PostMapping("/save")
    public void save(@RequestBody SomeEntity entity) {
        service.save(entity);
    }

    @PutMapping("/update/{id}")
    public void update(@PathVariable long id,
                       @RequestBody SomeEntity entity) {
        service.update(id, entity);
    }

    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable long id) {
        service.delete(id);
    }
}

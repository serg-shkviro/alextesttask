package com.javapadawan.alextesttask.domain;

public enum SortOrder {
    NONE,
    ASCENDING,
    DESCENDING
}

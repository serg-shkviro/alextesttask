package com.javapadawan.alextesttask.domain;

import javax.persistence.*;

/*
* assume SomeEntity is a side object,
* so its properties are valid on their own
* and are not to be deleted with the SomeEntity object
*/
@Entity
public class SomeEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @ManyToOne(fetch = FetchType.EAGER)
    private Document document;
    @ManyToOne(fetch = FetchType.EAGER)
    private DictionaryValue dictionaryValue;
    private SortOrder sortOrder;
    private long testId;
    private String testName;

    public SomeEntity() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }

    public DictionaryValue getDictionaryValue() {
        return dictionaryValue;
    }

    public void setDictionaryValue(DictionaryValue dictionaryValue) {
        this.dictionaryValue = dictionaryValue;
    }

    public SortOrder getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(SortOrder sortOrder) {
        this.sortOrder = sortOrder;
    }

    public long getTestId() {
        return testId;
    }

    public void setTestId(long testId) {
        this.testId = testId;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }
}

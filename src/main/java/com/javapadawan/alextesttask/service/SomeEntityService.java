package com.javapadawan.alextesttask.service;

import com.javapadawan.alextesttask.domain.SomeEntity;
import com.javapadawan.alextesttask.repository.SomeEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SomeEntityService {
    @Autowired
    private SomeEntityRepository repository;

    public List<SomeEntity> getAll() {
        return repository.findAll();
    }

    public void save(SomeEntity entity) {
        Optional<SomeEntity> entityOptional = repository.findById(entity.getId());
        if (entityOptional.isEmpty()) {
            repository.save(entity);
        }
    }

    public void update(long id, SomeEntity entity) {
        Optional<SomeEntity> entityOptional = repository.findById(id);
        if (entityOptional.isPresent()) {
            SomeEntity entityToUpdate = entityOptional.get();
            entityToUpdate.setSortOrder(entity.getSortOrder());
            entityToUpdate.setTestId(entity.getTestId());
            entityToUpdate.setTestName(entity.getTestName());
            entityToUpdate.setDocument(entity.getDocument());
            entityToUpdate.setDictionaryValue(entity.getDictionaryValue());
            repository.save(entityToUpdate);
        }
    }

    public void delete(long id) {
        Optional<SomeEntity> entityOptional = repository.findById(id);
        if (entityOptional.isPresent()) {
            repository.deleteById(entityOptional.get().getId());
        }
    }

    public SomeEntity getById(long id) {
        Optional<SomeEntity> entityOptional = repository.findById(id);
        if (entityOptional.isPresent()) {
            return entityOptional.get();
        } else {
            return null;
        }
    }
}
